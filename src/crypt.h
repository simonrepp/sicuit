#ifndef SICUIT_CRYPT_H
#define SICUIT_CRYPT_H

// Required by GPGME to determine large file system (LFS) support
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gpgme.h>
#include <stdbool.h>


struct CryptContext {
    gpgme_ctx_t gpgme_ctx;
    gpgme_error_t gpgme_err;
    size_t num_recipient_keys;
    gpgme_key_t *recipient_keys;
};

bool add_recipient_key(struct CryptContext *context, char *pattern);
bool decrypt_file_to_file(struct CryptContext *context, FILE *store_file, FILE *temp_file);
bool decrypt_file_to_memory(
    struct CryptContext *context,
    FILE *store_file,
    char **store_buffer,
    size_t *store_size
);
bool encrypt_file_to_file(struct CryptContext *context, FILE *temp_file, FILE *store_file);
bool encrypt_memory_to_file(
    struct CryptContext *context,
    const char *buffer,
    size_t size,
    FILE *store_file
);
bool init_crypt_context(struct CryptContext *context);
void release_crypt_context(struct CryptContext *context);

#endif
