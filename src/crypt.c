#include <locale.h>
#include <stdlib.h>
#include <string.h>

#include "crypt.h"

bool add_recipient_key(struct CryptContext *context, char *pattern)
{
    if ((context->gpgme_err = gpgme_op_keylist_start(context->gpgme_ctx, pattern, 0))) {
        fprintf(stderr,
                "GPG key listing could not be started.\n\n%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    context->num_recipient_keys++;
    context->recipient_keys = realloc(context->recipient_keys,
                                      (context->num_recipient_keys + 1) * sizeof(gpgme_key_t)); // + 1 for NULL-terminator
    context->recipient_keys[context->num_recipient_keys] = NULL; // note '[num] - 1' intentionally missing

    context->gpgme_err = gpgme_op_keylist_next(context->gpgme_ctx,
                                               &context->recipient_keys[context->num_recipient_keys - 1]);

    if (context->gpgme_err) {
        if (gpgme_err_code(context->gpgme_err) == GPG_ERR_EOF)
            fprintf(stderr, "No GPG key matching pattern '%s' was found.\n", pattern);
        else
            fprintf(stderr,
                    "GPG keylisting failed and the key for pattern '%s' could not be obtained.\n\n%s: %s\n",
                    pattern,
                    gpgme_strsource(context->gpgme_err),
                    gpgme_strerror(context->gpgme_err));

        return false;
    }

    gpgme_op_keylist_end(context->gpgme_ctx);

    // TODO: Test if this really does what we expect (= set truncated to 1 if we ended iteration early)
    if (gpgme_op_keylist_result(context->gpgme_ctx)->truncated) {
        fprintf(stderr, "Multiple GPG keys matching pattern '%s' were found, please try a more specific pattern.\n", pattern);
        return false;
    }

    return true;
}

bool decrypt_file_to_file(
    struct CryptContext *context,
    FILE *store_file,
    FILE *temp_file
) {
    gpgme_data_t gpgme_data_store;
    gpgme_data_t gpgme_data_temp_file;
    gpgme_decrypt_result_t gpgme_decrypt_result;
    gpgme_recipient_t gpgme_recipient;

    if ((context->gpgme_err = gpgme_data_new_from_stream(&gpgme_data_store, store_file))) {
        fprintf(stderr,
                "GPGME encrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_data_new_from_stream(&gpgme_data_temp_file, temp_file))) {
        fprintf(stderr,
                "GPGME decrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_op_decrypt(context->gpgme_ctx, gpgme_data_store, gpgme_data_temp_file))) {
        fprintf(stderr,
                "GPGME decryption failed.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    gpgme_decrypt_result = gpgme_op_decrypt_result(context->gpgme_ctx);

    if (gpgme_decrypt_result->unsupported_algorithm) {
        fprintf(stderr, "GPGME decryption failed - unsupported algorithm.\n");
        return false;
    }

    // If the user doesn't request keys to be used for encryption
    // we reuse those that the store is currently encrypted with
    if (context->num_recipient_keys == 0) {
        for (gpgme_recipient = gpgme_decrypt_result->recipients;
             gpgme_recipient != NULL;
             gpgme_recipient = gpgme_recipient->next) {
            context->num_recipient_keys++;
            context->recipient_keys = realloc(context->recipient_keys,
                                              (context->num_recipient_keys + 1) * sizeof(gpgme_key_t)); // + 1 for NULL-terminator
            context->recipient_keys[context->num_recipient_keys] = NULL; // note '[num] - 1' intentionally missing

            if ((context->gpgme_err = gpgme_get_key(context->gpgme_ctx, gpgme_recipient->keyid, &context->recipient_keys[context->num_recipient_keys - 1], 0))) {
                // TODO: More user-directed error - along the lines of "a public key that was previously used to encrypt the store is not available on this system to reencrypt the store"
                fprintf(stderr,
                        "GPG key with ID (fingerprint) '%s' could not be obtained from the GPGME backend.\n\n"
                        "%s: %s\n",
                        gpgme_recipient->keyid,
                        gpgme_strsource(context->gpgme_err),
                        gpgme_strerror(context->gpgme_err));
                // TODO: This skips gpgme_data_release_* at end of function
                return false;
            }
        }
    }

    gpgme_data_release(gpgme_data_temp_file);
    gpgme_data_release(gpgme_data_store);

    return true;
}

bool decrypt_file_to_memory(
    struct CryptContext *context,
    FILE *store_file,
    char **store_buffer,
    size_t *store_size
) {
    gpgme_data_t gpgme_data_store;
    gpgme_data_t gpgme_data_memory;

    if ((context->gpgme_err = gpgme_data_new_from_stream(&gpgme_data_store, store_file))) {
        fprintf(stderr,
                "GPGME encrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_data_new(&gpgme_data_memory))) {
        fprintf(stderr,
                "GPGME decrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_op_decrypt(context->gpgme_ctx, gpgme_data_store, gpgme_data_memory))) {
        fprintf(stderr,
                "GPGME decryption failed.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if (gpgme_op_decrypt_result(context->gpgme_ctx)->unsupported_algorithm) {
        fprintf(stderr, "GPGME decryption failed - unsupported algorithm.\n");
        return false;
    }

    *store_buffer = gpgme_data_release_and_get_mem(gpgme_data_memory, store_size);
    gpgme_data_release(gpgme_data_store);

    return true;
}

bool encrypt_file_to_file(struct CryptContext *context, FILE *temp_file, FILE *store_file) {
    gpgme_data_t gpgme_data_store;
    gpgme_data_t gpgme_data_temp_file;

    if ((context->gpgme_err = gpgme_data_new_from_stream(&gpgme_data_temp_file, temp_file))) {
        fprintf(stderr,
                "GPGME decrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_data_new_from_stream(&gpgme_data_store, store_file))) {
        fprintf(stderr,
                "GPGME encrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_op_encrypt(context->gpgme_ctx,
                                               context->recipient_keys,
                                               GPGME_ENCRYPT_ALWAYS_TRUST,
                                               gpgme_data_temp_file,
                                               gpgme_data_store))) {
        fprintf(stderr,
                "GPGME encryption failed.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if (gpgme_op_encrypt_result(context->gpgme_ctx)->invalid_recipients) {
        fprintf(stderr, "GPGME encryption failed - invalid recipients.\n");
        return false;
    }

    gpgme_data_release(gpgme_data_store);
    gpgme_data_release(gpgme_data_temp_file);

    return true;
}

bool encrypt_memory_to_file(
    struct CryptContext *context,
    const char *buffer,
    size_t size,
    FILE *store_file
) {
    gpgme_data_t gpgme_data_store;
    gpgme_data_t gpgme_data_memory;

    if ((context->gpgme_err = gpgme_data_new_from_mem(&gpgme_data_memory, buffer, size, 0))) {
        fprintf(stderr,
                "GPGME unencrypted memory data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_data_new_from_stream(&gpgme_data_store, store_file))) {
        fprintf(stderr,
                "GPGME encrypted data buffer could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_op_encrypt(context->gpgme_ctx,
                                               context->recipient_keys,
                                               GPGME_ENCRYPT_ALWAYS_TRUST,
                                               gpgme_data_memory,
                                               gpgme_data_store))) {
        fprintf(stderr,
                "GPGME encryption failed.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if (gpgme_op_encrypt_result(context->gpgme_ctx)->invalid_recipients) {
        fprintf(stderr, "GPGME encryption failed - invalid recipients.\n");
        return false;
    }

    gpgme_data_release(gpgme_data_store);
    gpgme_data_release(gpgme_data_memory);

    return true;
}

bool init_crypt_context(struct CryptContext *context)
{
    gpgme_check_version(NULL);
    gpgme_set_locale(NULL, LC_CTYPE, setlocale(LC_CTYPE, NULL));
    gpgme_set_locale(NULL, LC_MESSAGES, setlocale(LC_MESSAGES, NULL));

    if ((context->gpgme_err = gpgme_engine_check_version(GPGME_PROTOCOL_OPENPGP))) {
        fprintf(stderr,
                "The OpenPGP engine is not available (or of an incompatible version) in your environment.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    if ((context->gpgme_err = gpgme_new(&context->gpgme_ctx))) {
        fprintf(stderr,
                "GPGME context could not be initialized.\n\n"
                "%s: %s\n",
                gpgme_strsource(context->gpgme_err),
                gpgme_strerror(context->gpgme_err));
        return false;
    }

    // recipient_keys is a NULL-terminated array (it always holds num_recipient_keys + 1 references)
    context->num_recipient_keys = 0;
    context->recipient_keys = malloc(sizeof(gpgme_key_t));
    context->recipient_keys[0] = NULL;

    return true;
}

void release_crypt_context(struct CryptContext *context)
{
    for (size_t index = 0; index < context->num_recipient_keys; index++)
        gpgme_key_unref(context->recipient_keys[index]);

    gpgme_release(context->gpgme_ctx);
    free(context->recipient_keys);
}
