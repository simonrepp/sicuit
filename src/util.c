#include <dirent.h>
#include <pwd.h>
#include <stdlib.h>
#include <unistd.h>

#include "util.h"

char *get_editor()
{
    char *editor;

    if ((editor = getenv("EDITOR")) != NULL)
        return editor;

    if ((editor = getenv("VISUAL")) != NULL)
        return editor;

    if ((editor = getenv("SELECTED_EDITOR")) != NULL)
        return editor;

    // TODO: Manually inspect PATH instead of delegating to 'which' below?
    //       see e.g. https://stackoverflow.com/questions/41230547/check-if-program-is-installed-in-c

    if (system("which nano &> /dev/null") == EXIT_SUCCESS)
        return "nano";

    if (system("which nano-tiny &> /dev/null") == EXIT_SUCCESS)
        return "nano-tiny";

    if (system("which vi &> /dev/null") == EXIT_SUCCESS)
        return "vi";

    return NULL;
}

char *get_home_dir()
{
    char *home_dir = getenv("$XDG_CONFIG_HOME");
    if (home_dir != NULL) return home_dir;

    home_dir = getenv("HOME");
    if (home_dir != NULL) return home_dir;

    return getpwuid(getuid())->pw_dir;
}

/// Probes and returns one of the following locations, based on availability:
/// - "/dev/shm" (first priority - non-persistent by technical nature [tmpfs])
/// - "/run" (second priority - non-persistent by specification [tmpfs or removal/truncation at next boot])
/// - "/tmp" (always available - non-persistent by chance [completely unspecified])
char *get_temp_dir()
{
    DIR* dir_handle;

    if ((dir_handle = opendir("/dev/shm")) != NULL) {
        closedir(dir_handle);
        return "/dev/shm";
    }

    if ((dir_handle = opendir("/run")) != NULL) {
        closedir(dir_handle);
        return "/run";
    }

    return "/tmp";
}

/// Returns the number of digits in a number (42 => 2 digits, 569 => 3 digits, etc.)
///
/// Implemented only for numbers with up to 5 digits, as the numbers coming in here
/// relate to counts of results/entries in the store and we don't assume a usage
/// scenario where sicuit is used to manage more than 99999 secret entries.
unsigned int num_digits(unsigned int number)
{
    if (number < 10) return 1;
    if (number < 100) return 2;
    if (number < 1000) return 3;
    if (number < 10000) return 4;

    return 5;
}
