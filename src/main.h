#ifndef SICUIT_MAIN_H
#define SICUIT_MAIN_H

// Required by ncursesw (emphasis on w for wide character support)
// Also this MUST go before any other includes, or bad things (can) happen,
// as this also affects other system components
#define _XOPEN_SOURCE_EXTENDED

#include <eno.h>
#include <stdbool.h>
#include <unicode/ubrk.h>
#include <unicode/ucol.h>

#include "crypt.h"

#define CHARACTER_ARROW "→" // U+2192 Rightwards Arrow
#define CHARACTER_ELLIPSIS "…" // U+2026 Horizontal Ellipsis
#define CHARACTER_HIDDEN "·" // U+00B7 Middle Dot

#define PAIR_BLACK_FG_WHITE_BG 1
#define PAIR_BLACK_FG_YELLOW_BG 2
#define PAIR_CYAN_FG_DEFAULT_BG 3
#define PAIR_CYAN_FG_WHITE_BG 4
#define PAIR_YELLOW_FG_DEFAULT_BG 5
#define PAIR_YELLOW_FG_WHITE_BG 6

#define ATTR_RESULT WA_DIM
#define ATTR_RESULT_MATCHED COLOR_PAIR(PAIR_YELLOW_FG_DEFAULT_BG)
#define ATTR_RESULT_SECRET (WA_DIM | COLOR_PAIR(PAIR_BLACK_FG_YELLOW_BG))
#define ATTR_RESULT_SPECIAL (WA_DIM | COLOR_PAIR(PAIR_CYAN_FG_DEFAULT_BG))
#define ATTR_RESULT_SELECTED COLOR_PAIR(PAIR_BLACK_FG_WHITE_BG)
#define ATTR_RESULT_SELECTED_MATCHED (WA_BOLD | COLOR_PAIR(PAIR_YELLOW_FG_WHITE_BG))
#define ATTR_RESULT_SELECTED_SPECIAL (WA_DIM | COLOR_PAIR(PAIR_CYAN_FG_WHITE_BG))

#define GPG_KEY_FINGERPRINT_BUFFER_SIZE 41
#define PRINT_BUFFER_SIZE 1024
#define QUERY_STRING_MAX_NUM_BYTES 256
#define QUERY_STRING_MAX_NUM_CHARACTERS 64
#define RESULTS_INITIAL_CAPACITY 128

const char DEFAULT_STORE[] =
    "> This is your sicuit password store\n"
    "> For usage instructions go to https://codeberg.org/simonrepp/sicuit\n"
    "\n"
    "# Example Category\n"
    "\n"
    "alice@example.com: password_of_alice\n"
    "\n"
    "## Example Subcategory\n"
    "\n"
    "bob@example.com:\n"
    "Password = password_of_bob\n"
    "Server = imap.example.com\n"
    "User = bob\n"
;

const char HELP_TEXT[] = // %s serve to insert the executable as the caller sees it
    "USAGE\n"
    "   %s [STORE_PATH]\n"
    "   %s --edit [--for OWNER] … [STORE_PATH]\n"
    "   %s --init --for OWNER … [STORE_PATH]\n"
    "   %s --help\n"
    "   %s --version\n\n"
    "OPTIONS\n"
    "   Without flags\n"
    "       Query the store, see QUERY INTERFACE below\n\n"
    "   STORE_PATH\n"
    "       Optionally provide this in order to access a different store than the default one located at ~/.sicuit.eno.gpg\n\n"
    "   --edit, -e\n"
    "       Edit the store in your configured texteditor\n"
    "       Can be combined with --for to change the store owner(s) as well\n\n"
    "   --for OWNER, -f OWNER\n"
    "       Optional with the --edit flag to change who can access the store\n"
    "       Required with the --init flag to specify who can access the store\n\n"
    "       OWNER identifies a GPG key by name or email, e.g. --for alice@example.com or --for \"Jane Doe\"\n\n"
    "       This argument can be repeated to specify multiple owners for a store\n\n"
    "   --init, -i\n"
    "       Initialize the store\n"
    "       Must be combined with --for to specify the store owner(s)\n\n"
    "   --help, -h\n"
    "       Print this help text\n\n"
    "   --version, -v\n"
    "       Print version\n\n"
    "QUERY INTERFACE\n"
    "   The query interface starts in private mode, that is, any user input and all text\n"
    "   is concealed, allowing you to filter the store down to specific entries without\n"
    "   revealing anything to anyone that might be watching. You can gradually reveal\n"
    "   more information by pressing TAB to leave private mode (which reveals all\n"
    "   entry names, but no secrets), or SHIFT+TAB to peek at the currently selected entry\n"
    "   (which reveals the secret). ENTER prints the selected entry's secret to stdout.\n\n"
    "   TYPE:          Filter results\n"
    "   TAB:           Toggle private mode\n"
    "   SHIFT+TAB:     Peek at a secret\n"
    "   ENTER:         Print secret to stdout\n"
    "   ESCAPE:        Exit the program\n"
    "   UP/DOWN:       Navigate\n"
    "   PAGE UP/DOWN:  Navigate pagewise\n"
    "   HOME/END:      Go to first/last result\n\n"
    "EXAMPLES\n"
    "   sicuit\n"
    "   sicuit repos/NIMH/secret.eno.gpg\n"
    "   sicuit --init --for alice@example.com\n"
    "   sicuit --init --for alice@example.com --for jane@example.com repos/NIMH/secret.eno.gpg \n"
    "   sicuit --edit\n"
    "   sicuit --edit repos/NIMH/secret.eno.gpg\n"
    "   sicuit --edit --for alice@example.com --for jane@example.com\n\n"
;

struct Match {
    int depth; // -1 => No query ("blanket match")
    unsigned int offset;
};

struct Result {
    unsigned int depth;
    bool is_comment;
    struct Match match;
    unsigned int node_offsets[16]; // TODO: Consider making this dynamic to allow unlimited section depth
};

struct DrawContext {
    unsigned int available_columns;
    unsigned int depth;
    ENOIterator iterator;
    struct Result *result;
    bool selected;
};

struct FilterContext {
    unsigned int depth;
    ENOIterator iterator;
    unsigned int node_offsets[16]; // TODO: Consider making this dynamic to allow unlimited section depth
};

struct PrintContext {
    unsigned int depth;
    ENOIterator iterator;
    struct Result *result;
};

struct QueryContext {
    unsigned int available_columns;
    unsigned int available_lines;
    unsigned int display_offset;
    bool hide_keys;
    char *locale;
    unsigned int num_results;
    bool peek;
    char query_string[QUERY_STRING_MAX_NUM_BYTES];
    unsigned int query_string_num_bytes;
    unsigned int query_string_num_characters;
    struct Result *results;
    size_t results_capacity;
    int selected_index;
    ENOIterator* store_iterator;
    UCollator *unicode_collator;
    UBreakIterator *unicode_break_iterator;
    UErrorCode unicode_status;
    UText *unicode_text;
};

bool edit_store(char *store_path, struct CryptContext *crypt_context, char *argv0);
bool init_store(char *store_path, struct CryptContext *crypt_context);
void migrate_legacy_store(char *home_dir, char *new_store_file_path);
bool query_store(char *store_path, struct CryptContext *crypt_context);
void query_store_draw_all(struct QueryContext *context);
void query_store_draw_query(struct QueryContext *context);
void query_store_draw_result_recursive(struct QueryContext *context, struct DrawContext *draw_context);
void query_store_draw_results(struct QueryContext *context);
void query_store_draw_secret(struct QueryContext *context, struct DrawContext *draw_context, char* secret, size_t secret_num_bytes);
void query_store_ensure_results_capacity(struct QueryContext *context);
void query_store_filter_results(struct QueryContext *context);
void query_store_filter_results_recursive(struct QueryContext *context, struct FilterContext *filter_context, struct Match *earlier_match);
void query_store_print_secret(struct QueryContext *context);
void query_store_print_secret_recursive(struct QueryContext *context, struct PrintContext *print_context);
void query_store_query_pop(struct QueryContext *context);
void query_store_query_push(struct QueryContext *context, int *input_keycode);
void query_store_reposition_cursor(struct QueryContext *context);
void query_store_resize_window(struct QueryContext *context);
void query_store_select_first(struct QueryContext *context);
void query_store_select_last(struct QueryContext *context);
void query_store_select_next(struct QueryContext *context);
void query_store_select_next_page(struct QueryContext *context);
void query_store_select_previous(struct QueryContext *context);
void query_store_select_previous_page(struct QueryContext *context);

#endif
