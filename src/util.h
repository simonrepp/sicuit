#ifndef SICUIT_UTIL_H
#define SICUIT_UTIL_H

char *get_editor();
char *get_home_dir();
char *get_temp_dir();
unsigned int num_digits(unsigned int number);

#endif
