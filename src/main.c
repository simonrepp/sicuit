#include <errno.h>
#include <locale.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "main.h"
#include "util.h"

bool edit_store(char *store_path, struct CryptContext *crypt_context, char *sicuit_invocation)
{
    char *editor = get_editor();
    char editor_invocation[PRINT_BUFFER_SIZE];
    FILE *store_file;
    char temp_path[PRINT_BUFFER_SIZE];
    bool success;
    char *temp_dir = get_temp_dir();
    FILE *temp_file;

    if (editor == NULL) {
        fprintf(stderr,
                "Your preferred editor could not be determined:\n"
                "- The environment variables EDITOR, VISUAL, SELECTED_EDITOR are empty\n"
                "- Neither of nano, nano-tiny or vi exist in any of the directories in PATH\n\n"
                "You can prepend like 'EDITOR=insert_your_editor %s ...' to temporarily specify an editor.\n",
                sicuit_invocation);
        return false;
    }

    sprintf(temp_path, "%s/sicuit.eno", temp_dir);

    if ((success = ((store_file = fopen(store_path, "rb")) != NULL))) {
        if ((success = ((temp_file = fopen(temp_path, "wb")) != NULL))) {
            success = decrypt_file_to_file(crypt_context, store_file, temp_file);
            fclose(temp_file);
        } else {
            fprintf(stderr,
                    "Could not create the temporary file for editing the store at '%s'.\n",
                    temp_path);
        }
        fclose(store_file);
    } else {
        fprintf(stderr,
                "Could not open the store at '%s'.\n"
                "Make sure it exists and has appropriate permissions set.\n", // TODO: Mention the critically required permissions
                store_path);
    }

    if (!success)
        return false;

    sprintf(editor_invocation, "%s %s/sicuit.eno", editor, temp_dir);
    system(editor_invocation);

    if ((success = ((temp_file = fopen(temp_path, "rb")) != NULL))) {
        if ((success = ((store_file = fopen(store_path, "wb")) != NULL))) {
            success = encrypt_file_to_file(crypt_context, temp_file, store_file);
            fclose(store_file);
        } else {
            fprintf(stderr, "Could not open the store at '%s' for writing.\n", store_path);
        }

        fclose(temp_file);
    } else {
        fprintf(stderr, "Could not open the temporary file at '%s'.\n", temp_path);
    }

    remove(temp_path);

    if (!success) return false;

    printf("The store has been updated.\n");
    return true;
}

bool init_store(char *store_path, struct CryptContext *crypt_context)
{
    FILE *store_file;    

    if (crypt_context->num_recipient_keys == 0) {
        printf("You need to specify at least one GPG key with which to encrypt the store (using --for), see --help for usage instructions.\n");
        return false;
    }

    store_file = fopen(store_path, "rb");

    if (store_file != NULL) {
        fclose(store_file);
        fprintf(stderr,
                "A previously initialized store at '%s' already exists.\n"
                "If you really want to replace it, remove it manually, then re-run sicuit.\n",
                store_path);
        return false;
    }

    store_file = fopen(store_path, "wb");

    if (store_file == NULL) {
        fprintf(stderr,
                "The store file at '%s' cannot be written to.\n"
                "Make sure that your current user has adequate permissions, then re-run 'sicuit --init'.\n",
                store_path);
        return false;
    }

    if (!encrypt_memory_to_file(crypt_context,
                                DEFAULT_STORE,
                                strlen(DEFAULT_STORE),
                                store_file))
        return false;

    fclose(store_file);

    printf("The store has been initialized at '%s'.\n", store_path);
    return true;
}

int main(int argc, char **argv)
{
    struct CryptContext crypt_context;
    char *home_dir = get_home_dir();
    enum Mode {EDIT, INIT, QUERY} mode = QUERY;
    char store_path[PRINT_BUFFER_SIZE] = "";

    setlocale(LC_ALL, "");

    if (argc > 1) {
        if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
            printf(HELP_TEXT, argv[0], argv[0], argv[0], argv[0], argv[0]);
            return EXIT_SUCCESS;
        }

        if (strcmp(argv[1], "-v") == 0 || strcmp(argv[1], "--version") == 0) {
            printf("sicuit beta " VERSION "\n");
            return EXIT_SUCCESS;
        }
    }

    if (!init_crypt_context(&crypt_context))
        return EXIT_FAILURE;

    for (int i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            strcpy(store_path, argv[i]);
        } else if (strcmp(argv[i], "-e") == 0 || strcmp(argv[i], "--edit") == 0) {
            mode = EDIT;
        } else if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--for") == 0) {
            if (++i == argc) {
                fprintf(stderr, "Argument --for given without recipient, see --help for usage instructions.\n");
                goto fail;
            }

            if (!add_recipient_key(&crypt_context, argv[i]))
                goto fail;
        } else if (strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "--init") == 0) {
            mode = INIT;
        } else {
            fprintf(stderr, "Unrecognized argument '%s', see --help for usage instructions.\n", argv[i]);
            goto fail;
        }
    }

    if (store_path[0] == '\0') {
        sprintf(store_path, "%s/.sicuit.eno.gpg", home_dir);

        // TODO: Remove at a future point, including function declaration and definition
        migrate_legacy_store(home_dir, store_path);
    }

    if (mode == EDIT) {
        if (!edit_store(store_path, &crypt_context, argv[0]))
            goto fail;
    } else if (mode == INIT) {
        if (!init_store(store_path, &crypt_context))
            goto fail;
    } else if (mode == QUERY) {
        if (!query_store(store_path, &crypt_context))
            goto fail;
    }

    release_crypt_context(&crypt_context);
    return EXIT_SUCCESS;

    fail:
    release_crypt_context(&crypt_context);
    return EXIT_FAILURE;
}

void migrate_legacy_store(char *home_dir, char *new_store_file_path)
{
    char command_invocation[PRINT_BUFFER_SIZE * 2];
    FILE *file;
    char legacy_store_file_path[PRINT_BUFFER_SIZE];
    char unused_read;

    if ((file = fopen(new_store_file_path, "rb")) != NULL) {
        fclose(file);
        return; // store already at new location
    }

    sprintf(legacy_store_file_path, "%s/.sicuit/store.eno.gpg", home_dir);

    if ((file = fopen(legacy_store_file_path, "rb")) == NULL)
        return; // no store found at all

    fclose(file);

    fprintf(stderr,
            "sicuit is transitioning to a simpler convention for its files on disk:\n\n"
            "The encrypted secret file moves:   ~/.sicuit/store.eno.gpg => ~/.sicuit.eno.gpg\n"
            "All else is now obsolete:          ~/.sicuit/config.eno    => *can be deleted*\n"
            "                                   ~/.sicuit               => *can be deleted*\n\n"
            "Press ENTER to migrate automatically, or cancel with CTRL+ C if you like to migrate manually.\n");

    scanf("%c", &unused_read);

    sprintf(command_invocation, "mv %s %s", legacy_store_file_path, new_store_file_path);
    system(command_invocation);

    sprintf(command_invocation, "rm -rf %s/.sicuit/", home_dir);
    system(command_invocation);
}

bool query_store(char *store_path, struct CryptContext *crypt_context)
{
    struct QueryContext context;
    int input_keycode;
    char *store_buffer;
    FILE *store_file;
    ENOIterator store_iterator;
    size_t store_size;

    store_file = fopen(store_path, "rb");

    if (store_file == NULL) {
        fprintf(stderr,
                "Could not open the store at '%s'.\n"
                "Make sure it exists and has appropriate permissions set.\n", // TODO: Mention the critically required permissions
                store_path);
        return false;
    }

    if (!decrypt_file_to_memory(crypt_context, store_file, &store_buffer, &store_size)) {
        fclose(store_file);
        return false;
    }

    fclose(store_file);

    if (!eno_parse_memory(&store_iterator, store_buffer, store_size)) {
        eno_report_error(&store_iterator);
        eno_free_document(&store_iterator);
        gpgme_free(store_buffer);   // TODO: gpgme spills into main.c here (maybe tolerable because hard to avoid)
        return false;
    }

    if (!eno_has_children(&store_iterator)) {
        printf("The password store has no entries.\n");
        eno_free_document(&store_iterator);
        gpgme_free(store_buffer);
        return true;
    }

    context.display_offset = 0;
    context.hide_keys = true;
    context.locale = setlocale(LC_CTYPE, NULL);
    context.peek = false;
    context.query_string_num_bytes = 0;
    context.query_string_num_characters = 0;
    context.results_capacity = RESULTS_INITIAL_CAPACITY;
    context.selected_index = 0;
    context.store_iterator = &store_iterator;
    context.unicode_status = U_ZERO_ERROR;
    context.unicode_text = NULL;

    context.results = malloc(context.results_capacity * sizeof(*context.results));
    context.unicode_break_iterator = ubrk_open(UBRK_CHARACTER, context.locale, NULL, 0, &context.unicode_status);
    context.unicode_collator = ucol_open(NULL, &context.unicode_status);

    // compare only base characters (= disregard accent, case and variant)
    ucol_setStrength(context.unicode_collator, UCOL_PRIMARY);

    initscr();
    start_color();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    set_escdelay(0);

    use_default_colors();

    init_pair(PAIR_BLACK_FG_WHITE_BG, COLOR_BLACK, COLOR_WHITE);
    init_pair(PAIR_BLACK_FG_YELLOW_BG, COLOR_BLACK, COLOR_YELLOW);
    init_pair(PAIR_CYAN_FG_DEFAULT_BG, COLOR_CYAN, -1);
    init_pair(PAIR_CYAN_FG_WHITE_BG, COLOR_CYAN, COLOR_WHITE);
    init_pair(PAIR_YELLOW_FG_DEFAULT_BG, COLOR_YELLOW, -1);
    init_pair(PAIR_YELLOW_FG_WHITE_BG, COLOR_YELLOW, COLOR_WHITE);

    getmaxyx(stdscr, context.available_lines, context.available_columns);

    query_store_filter_results(&context);
    query_store_draw_all(&context);

    while (1) {
        input_keycode = getch();

        // Any keypress other than SHIFT+TAB disables selection exposure as a
        // panic exit when one accidentally exposes the selection
        if (context.peek && input_keycode != KEY_BTAB && input_keycode != KEY_RESIZE) {
            context.peek = false;
        }

        switch (input_keycode) {
            case 9: // TAB
                context.hide_keys = !context.hide_keys;
                break;
            case 10: // ENTER
                endwin();
                query_store_print_secret(&context);
                goto end;
            case 27: // ESC
                endwin();
                goto end;
            case KEY_BACKSPACE: // fall-through
            case 127: // fall-through
            case '\b':
                query_store_query_pop(&context);
                break;
            case KEY_BTAB: // SHIFT+TAB
                context.peek = !context.peek;
                break;
            case KEY_DC: // fall-through
            case KEY_LEFT: // fall-through
            case KEY_RIGHT:
                break;  // TODO: Generic way to discard unused non-character keys?
            case KEY_END:
                query_store_select_last(&context);
                break;
            case KEY_DOWN:
                query_store_select_next(&context);
                break;
            case KEY_HOME:
                query_store_select_first(&context);
                break;
            case KEY_NPAGE:
                query_store_select_next_page(&context);
                break;
            case KEY_PPAGE:
                query_store_select_previous_page(&context);
                break;
            case KEY_RESIZE:
                query_store_resize_window(&context);
                break;
            case KEY_UP:
                query_store_select_previous(&context);
                break;
            default:
                query_store_query_push(&context, &input_keycode);
        }

        query_store_draw_all(&context);
    }

    end:
    free(context.results);

    ubrk_close(context.unicode_break_iterator);
    ucol_close(context.unicode_collator);
    utext_close(context.unicode_text);

    eno_free_document(&store_iterator);

    gpgme_free(store_buffer);

    return true;
}

void query_store_draw_all(struct QueryContext *context)
{
    query_store_draw_query(context);
    query_store_draw_results(context);
    query_store_reposition_cursor(context);

    refresh();
}

void query_store_draw_query(struct QueryContext *context)
{
    unsigned int available_columns = context->available_columns;

    move(0, 0);

    if (context->query_string_num_characters > 0) {
        int32_t key_byte_offset;
        int32_t previous_key_byte_offset = 0;

        context->unicode_text = utext_openUTF8(context->unicode_text,
                                               context->query_string,
                                               context->query_string_num_bytes,
                                               &context->unicode_status);

        ubrk_setUText(context->unicode_break_iterator,
                      context->unicode_text,
                      &context->unicode_status);

        while ((key_byte_offset = ubrk_next(context->unicode_break_iterator)) != UBRK_DONE) {
            if (available_columns == 2 && key_byte_offset != (int32_t)context->query_string_num_bytes) {
                printw(CHARACTER_ELLIPSIS);
                break;
            }

            if (context->hide_keys) {
                printw(CHARACTER_HIDDEN);
            } else {
                printw("%.*s", key_byte_offset - previous_key_byte_offset, context->query_string + previous_key_byte_offset);
            }
            available_columns--;

            previous_key_byte_offset = key_byte_offset;
        }
    }

    clrtoeol();

    // Print "[selected]/[total]" in top-right corner if there is space
    attron(WA_DIM);
    if (context->num_results > 0) {
        unsigned int required_columns = num_digits(context->selected_index + 1) + 1 + num_digits(context->num_results); // "[n]/[m]"

        if (available_columns >= required_columns + 2)
            mvprintw(0,
                     context->available_columns - required_columns,
                     "%d/%d",
                     context->selected_index + 1,
                     context->num_results);
    } else if (available_columns >= 5) {
        mvprintw(0, context->available_columns - 3, "0/0");
    }
    attroff(WA_DIM);
}

void query_store_draw_result_recursive(struct QueryContext *context, struct DrawContext *draw_context)
{
    unsigned int node_offset = 0;

    // Iterate to the node we need
    while (eno_iterate_next(&draw_context->iterator) &&
           node_offset != draw_context->result->node_offsets[draw_context->depth])
        node_offset++;

    // Print " ([n])" if applies
    if (eno_is_item(&draw_context->iterator)) {
        if (context->peek && draw_context->selected) {
            char *value;
            size_t value_num_bytes;

            eno_get_value(&draw_context->iterator, &value, &value_num_bytes);
            query_store_draw_secret(context, draw_context, value, value_num_bytes);
        } else {
            int attr = draw_context->selected ? ATTR_RESULT_SELECTED_SPECIAL : ATTR_RESULT_SPECIAL;
            unsigned int number = node_offset + 1;
            unsigned int required_columns = strlen(" ()") + num_digits(number);

            // Clipping behavior:
            // - "example.com █(1)" => "example.com…█"
            // - "example.com (█1)" => "example.com …█"
            // - "example.com (1█)" => "example.com (…█"

            attron(attr);
            if (draw_context->available_columns >= required_columns) {
                printw(" (%d)", number);
                draw_context->available_columns -= required_columns;
            } else {
                if (draw_context->available_columns == 1) {
                    printw(CHARACTER_ELLIPSIS);
                    draw_context->available_columns = 0;
                } else if (draw_context->available_columns == 2) {
                    printw(" " CHARACTER_ELLIPSIS);
                    draw_context->available_columns = 0;
                } else /* if (draw_context->available_columns >= 3) */ {
                    printw(" (" CHARACTER_ELLIPSIS);
                    draw_context->available_columns -= 3;
                }
            }
            attroff(attr);
        }

        return;
    }

    if (!(context->peek && draw_context->selected)) {
        // Print " → " if applies
        if (draw_context->depth > 0) {
            int attr = draw_context->selected ? ATTR_RESULT_SELECTED : ATTR_RESULT;

            // Clipping behavior:
            // - "example.com █→ password" => "example.com…█"
            // - "example.com →█ password" => "example.com …█"
            // - "example.com → █password" => "example.com … █"

            attron(attr);
            if (draw_context->available_columns > 3) {
                printw(" " CHARACTER_ARROW " ");
                draw_context->available_columns -= 3;
                attroff(attr);
            } else {
                if (draw_context->available_columns == 3) {
                    printw(" " CHARACTER_ELLIPSIS " ");
                    draw_context->available_columns -= 3;
                } else if (draw_context->available_columns == 2) {
                    printw(" " CHARACTER_ELLIPSIS);
                    draw_context->available_columns -= 2;
                } else /* if (draw_context->available_columns == 1) */ {
                    printw(CHARACTER_ELLIPSIS);
                    draw_context->available_columns -= 1;
                }

                attroff(attr);
                return;
            }
        }

        // Print "[key]"
        char *key;
        size_t key_num_bytes;
        int32_t key_byte_offset = 0;
        int32_t key_character_offset = 0;
        int32_t previous_key_byte_offset = 0;

        eno_get_key(&draw_context->iterator, &key, &key_num_bytes);

        context->unicode_text = utext_openUTF8(context->unicode_text,
                                               key,
                                               key_num_bytes,
                                               &context->unicode_status);

        ubrk_setUText(context->unicode_break_iterator,
                      context->unicode_text,
                      &context->unicode_status);

        while ((key_byte_offset = ubrk_next(context->unicode_break_iterator)) != UBRK_DONE) {
            bool matched =
                (int)draw_context->depth == draw_context->result->match.depth &&
                key_character_offset >= (int)draw_context->result->match.offset &&
                key_character_offset < (int)(draw_context->result->match.offset + context->query_string_num_characters);

            int attr = matched ?
                (draw_context->selected ? ATTR_RESULT_SELECTED_MATCHED : ATTR_RESULT_MATCHED) :
                (draw_context->selected ? ATTR_RESULT_SELECTED : ATTR_RESULT);

            // Clipping behavior:
            // - "example.co█m" => "example.c…█"
            // - "example.com█ → password" => "example.co…█"
            // - "example.com█ (notes)" => "example.co…█"

            attron(attr);
            if (draw_context->available_columns == 1 &&
                ((size_t)key_byte_offset < key_num_bytes ||
                 draw_context->depth != draw_context->result->depth ||
                 draw_context->result->is_comment)) {
                printw(CHARACTER_ELLIPSIS);
                draw_context->available_columns--;
                attroff(attr);
                return;
            } else if (context->hide_keys) {
                printw(CHARACTER_HIDDEN);
            } else {
                printw("%.*s", key_byte_offset - previous_key_byte_offset, key + previous_key_byte_offset);
            }
            draw_context->available_columns--;
            attroff(attr);

            previous_key_byte_offset = key_byte_offset;
            key_character_offset++;
        }
    }

    if (draw_context->available_columns == 0)
        return;

    // Print " (notes)" if applies
    if (draw_context->depth == draw_context->result->depth) {
        if (draw_context->result->is_comment) {
            if (context->peek && draw_context->selected) {
                char *comment;
                size_t comment_num_bytes;

                eno_get_comment(&draw_context->iterator, &comment, &comment_num_bytes);
                query_store_draw_secret(context, draw_context, comment, comment_num_bytes);
            } else {
                int attr = draw_context->selected ? ATTR_RESULT_SELECTED_SPECIAL : ATTR_RESULT_SPECIAL;
                unsigned int required_columns = strlen(" (notes)");

                // Clipping behavior:
                // - "example.com █(notes)" => "example.com…█"
                // - "example.com (█notes)" => "example.com …█"
                // - "example.com (notes█)" => "example.com (…█"

                attron(attr);
                if (draw_context->available_columns >= required_columns) {
                    printw(" (notes)");
                    draw_context->available_columns -= required_columns;
                } else {
                    if (draw_context->available_columns == 1) {
                        printw(CHARACTER_ELLIPSIS);
                        draw_context->available_columns = 0;
                    } else if (draw_context->available_columns == 2) {
                        printw(" " CHARACTER_ELLIPSIS);
                        draw_context->available_columns = 0;
                    } else /* if (draw_context->available_columns >= 3) */ {
                        printw(" (" CHARACTER_ELLIPSIS);
                        draw_context->available_columns -= 3;
                    }
                }
                attroff(attr);
            }
        } else if (context->peek && draw_context->selected) {
            char *value;
            size_t value_num_bytes;

            eno_get_value(&draw_context->iterator, &value, &value_num_bytes);
            query_store_draw_secret(context, draw_context, value, value_num_bytes);
        }

        return;
    }

    if (eno_has_children(&draw_context->iterator)) {
        draw_context->depth++;
        eno_iterate_children(&draw_context->iterator, &draw_context->iterator);

        query_store_draw_result_recursive(context, draw_context);
    }
}

void query_store_draw_results(struct QueryContext *context)
{
    struct DrawContext draw_context;
    unsigned int result_index = context->display_offset;

    while (result_index < context->num_results &&
           result_index < context->display_offset + (context->available_lines - 2)) {
        draw_context.available_columns = context->available_columns;
        draw_context.depth = 0;
        draw_context.iterator = *context->store_iterator;
        draw_context.result = &context->results[result_index];
        draw_context.selected = (int)result_index == context->selected_index;

        move(result_index - context->display_offset + 2, 0);

        query_store_draw_result_recursive(context, &draw_context);

        if (draw_context.selected) {
            int attr = context->peek ? ATTR_RESULT_SECRET : ATTR_RESULT_SELECTED;

            attron(attr);
            while (draw_context.available_columns > 0) {
                printw(" ");
                draw_context.available_columns--;
            }
            attroff(attr);
        } else {
            clrtoeol();
        }

        result_index++;
    }

    if (context->num_results < context->available_lines - 2)
        clrtobot();
}

void query_store_draw_secret(struct QueryContext *context, struct DrawContext *draw_context, char* secret, size_t secret_num_bytes)
{
    int32_t secret_byte_offset = 0;
    int32_t previous_secret_byte_offset = 0;

    context->unicode_text = utext_openUTF8(context->unicode_text,
                                           secret,
                                           secret_num_bytes,
                                           &context->unicode_status);

    ubrk_setUText(context->unicode_break_iterator,
                  context->unicode_text,
                  &context->unicode_status);

    attron(ATTR_RESULT_SECRET);
    while ((secret_byte_offset = ubrk_next(context->unicode_break_iterator)) != UBRK_DONE) {
        if (draw_context->available_columns == 1 && (size_t)secret_byte_offset < secret_num_bytes) {
            printw(CHARACTER_ELLIPSIS);
            draw_context->available_columns--;
            break;
        } else if (*(secret + previous_secret_byte_offset) == '\n') {
            printw("⏎"); // indicate newline but continue inline
        } else {
            printw("%.*s", secret_byte_offset - previous_secret_byte_offset, secret + previous_secret_byte_offset);
        }
        draw_context->available_columns--;

        previous_secret_byte_offset = secret_byte_offset;
    }
    attroff(ATTR_RESULT_SECRET);
}

void query_store_ensure_results_capacity(struct QueryContext *context)
{
    if (context->num_results == context->results_capacity) {
        struct Result *results_memo = context->results;

        context->results_capacity *= 2;
        context->results = realloc(context->results, context->results_capacity * sizeof(*context->results));

        if (context->results == NULL) {
            // TODO: Gracefully exit program
            free(results_memo);
        }
    }
}

void query_store_filter_results(struct QueryContext *context)
{
    struct FilterContext filter_context;

    context->num_results = 0;
    filter_context.depth = 0;
    filter_context.iterator = *context->store_iterator;

    if (context->query_string_num_characters == 0) {
        struct Match blanket_match;

        blanket_match.depth = -1;

        query_store_filter_results_recursive(context, &filter_context, &blanket_match);
    } else {
        query_store_filter_results_recursive(context, &filter_context, NULL);
    }
}

void query_store_filter_results_recursive(struct QueryContext *context, struct FilterContext *filter_context, struct Match *earlier_match)
{
    int32_t byte_offset;
    UCollationResult result;
    char *key;
    size_t key_num_bytes;
    struct Match local_match;

    filter_context->node_offsets[filter_context->depth] = 0;

    while (eno_iterate_next(&filter_context->iterator)) {
        bool is_match = earlier_match != NULL;

        if (earlier_match == NULL && eno_has_key(&filter_context->iterator)) {
            eno_get_key(&filter_context->iterator, &key, &key_num_bytes);

            context->unicode_text = utext_openUTF8(context->unicode_text,
                                                   key,
                                                   key_num_bytes,
                                                   &context->unicode_status);

            ubrk_setUText(context->unicode_break_iterator,
                          context->unicode_text,
                          &context->unicode_status);

            byte_offset = 0;
            unsigned int character_offset = 0;

            while (key_num_bytes - byte_offset >= context->query_string_num_bytes) {
                result = ucol_strcollUTF8(context->unicode_collator,
                                          key + byte_offset,
                                          context->query_string_num_bytes,
                                          context->query_string,
                                          context->query_string_num_bytes,
                                          &context->unicode_status);

                if (result == UCOL_EQUAL) {
                    local_match.offset = character_offset;
                    local_match.depth = filter_context->depth;
                    is_match = true;
                    break;
                }

                if ((byte_offset = ubrk_next(context->unicode_break_iterator)) == UBRK_DONE)
                    break;

                character_offset++;
            }
        }

        if (is_match && eno_has_comment(&filter_context->iterator)) {
            query_store_ensure_results_capacity(context);

            context->results[context->num_results].depth = filter_context->depth;
            memcpy(context->results[context->num_results].node_offsets, filter_context->node_offsets, sizeof(filter_context->node_offsets));
            context->results[context->num_results].is_comment = true;

            if (earlier_match == NULL) {
                context->results[context->num_results].match = local_match;
            } else {
                context->results[context->num_results].match = *earlier_match;
            }

            context->num_results++;
        }

        if (eno_has_children(&filter_context->iterator)) {
            struct FilterContext child_filter_context = *filter_context;

            eno_iterate_children(&filter_context->iterator, &child_filter_context.iterator);
            child_filter_context.depth++;

            if (earlier_match != NULL) {
                query_store_filter_results_recursive(context, &child_filter_context, earlier_match);
            } else if(is_match) {
                query_store_filter_results_recursive(context, &child_filter_context, &local_match);
            } else {
                query_store_filter_results_recursive(context, &child_filter_context, NULL);
            }
        } else if (is_match && eno_has_value(&filter_context->iterator)) {
            query_store_ensure_results_capacity(context);

            context->results[context->num_results].depth = filter_context->depth;
            memcpy(context->results[context->num_results].node_offsets, filter_context->node_offsets, sizeof(filter_context->node_offsets));
            context->results[context->num_results].is_comment = false;

            if (earlier_match == NULL) {
                context->results[context->num_results].match = local_match;
            } else {
                context->results[context->num_results].match = *earlier_match;
            }

            context->num_results++;
        }

        filter_context->node_offsets[filter_context->depth]++;
    }
}

void query_store_print_secret(struct QueryContext *context)
{
    struct PrintContext print_context;

    if (context->selected_index == -1)
        return;

    print_context.depth = 0;
    print_context.iterator = *context->store_iterator;
    print_context.result = &context->results[context->selected_index];

    query_store_print_secret_recursive(context, &print_context);
}

void query_store_print_secret_recursive(struct QueryContext *context, struct PrintContext *print_context)
{
    unsigned int node_offset = 0;

    while (eno_iterate_next(&print_context->iterator)) {
        if (node_offset != print_context->result->node_offsets[print_context->depth]) {
            node_offset++;
            continue;
        }

        if (print_context->result->is_comment &&
            print_context->depth == print_context->result->depth) {
            char *comment;
            size_t comment_num_bytes;

            eno_get_comment(&print_context->iterator, &comment, &comment_num_bytes);

            printf("%.*s", (unsigned int)comment_num_bytes, comment);

            return;
        }

        if (eno_has_children(&print_context->iterator)) {
            print_context->depth++;
            eno_iterate_children(&print_context->iterator, &print_context->iterator);

            query_store_print_secret_recursive(context, print_context);
        } else {
            char *value;
            size_t value_num_bytes;

            eno_get_value(&print_context->iterator, &value, &value_num_bytes);

            printf("%.*s", (unsigned int)value_num_bytes, value);
        }

        return;
    }
}

void query_store_query_pop(struct QueryContext *context)
{
    if (context->query_string_num_characters > 0) {
        context->unicode_text = utext_openUTF8(context->unicode_text,
                                              context->query_string,
                                              context->query_string_num_bytes,
                                              &context->unicode_status);

        ubrk_setUText(context->unicode_break_iterator,
                      context->unicode_text,
                      &context->unicode_status);

        ubrk_last(context->unicode_break_iterator);

        context->query_string_num_bytes = ubrk_previous(context->unicode_break_iterator);
        context->query_string_num_characters--;

        query_store_filter_results(context);

        if (context->selected_index == -1 && context->num_results > 0) {
            context->display_offset = 0;
            context->selected_index = 0;
        }
    }
}

void query_store_query_push(struct QueryContext *context, int *input_keycode)
{
    nodelay(stdscr, TRUE);
    do {
        if (context->query_string_num_bytes == QUERY_STRING_MAX_NUM_BYTES)
            goto discard_input;

        context->query_string[context->query_string_num_bytes++] = *input_keycode;

        *input_keycode = getch();
    } while (*input_keycode != ERR);
    nodelay(stdscr, FALSE);

    context->unicode_text = utext_openUTF8(context->unicode_text,
                                          context->query_string,
                                          context->query_string_num_bytes,
                                          &context->unicode_status);

    ubrk_setUText(context->unicode_break_iterator,
                  context->unicode_text,
                  &context->unicode_status);

    context->query_string_num_characters = 0;

    while (ubrk_next(context->unicode_break_iterator) != UBRK_DONE) {
        context->query_string_num_characters++;

        if (context->query_string_num_characters == QUERY_STRING_MAX_NUM_CHARACTERS)
            break;
    }

    discard_input:
    query_store_filter_results(context);

    if (context->selected_index >= (int)context->num_results)
        context->selected_index = context->num_results - 1;

    if (context->num_results > context->available_lines - 2) {
        if (context->display_offset + (context->available_lines - 2) >= context->num_results)
            context->display_offset = context->num_results - (context->available_lines - 2);
    } else {
        context->display_offset = 0;
    }
}

void query_store_reposition_cursor(struct QueryContext *context)
{
    if (context->available_columns > context->query_string_num_characters) {
        move(0, context->query_string_num_characters);
    } else {
        move(0, context->available_columns - 1);
    }
}

void query_store_resize_window(struct QueryContext *context)
{
    getmaxyx(stdscr, context->available_lines, context->available_columns);

    if (context->selected_index == -1 || context->available_lines < 3)
        return;

    if (context->selected_index - context->display_offset >= context->available_lines - 2)
        context->selected_index = context->display_offset + (context->available_lines - 2) - 1;
}

void query_store_select_first(struct QueryContext *context)
{
    if (context->num_results == 0)
        return;

    context->display_offset = 0;
    context->selected_index = 0;
}

void query_store_select_last(struct QueryContext *context)
{
    if (context->num_results == 0)
        return;

    if (context->num_results > context->available_lines - 2)
        context->display_offset = context->num_results - (context->available_lines - 2);

    context->selected_index = context->num_results - 1;
}

void query_store_select_next(struct QueryContext *context)
{
    if (context->num_results == 0 || context->selected_index + 1 == (int)context->num_results)
        return;

    context->selected_index++;

    if (context->selected_index - context->display_offset >= context->available_lines - 2)
        context->display_offset++;
}

void query_store_select_next_page(struct QueryContext *context)
{
    if (context->num_results == 0)
        return;

    if (context->num_results > context->available_lines - 2) {
        context->display_offset += context->available_lines - 2;
        if (context->display_offset + (context->available_lines - 2) >= context->num_results)
            context->display_offset = context->num_results - (context->available_lines - 2);

        context->selected_index += context->available_lines - 2;
        if (context->selected_index >= (int)context->num_results)
            context->selected_index = context->num_results - 1;
    } else {
        context->selected_index = context->num_results - 1;
    }
}

void query_store_select_previous(struct QueryContext *context)
{
    if (context->num_results == 0 || context->selected_index == 0)
        return;

    context->selected_index--;

    if (context->selected_index < (int)context->display_offset)
        context->display_offset = context->selected_index;
}

void query_store_select_previous_page(struct QueryContext *context)
{
    if (context->num_results == 0)
        return;

    if (context->num_results > context->available_lines - 2) {
        if (context->display_offset > (context->available_lines - 2))
            context->display_offset -= context->available_lines - 2;
        else
            context->display_offset = 0;


        context->selected_index -= context->available_lines - 2;
        if (context->selected_index < 0)
            context->selected_index = 0;
    } else {
        context->selected_index = 0;
    }
}
