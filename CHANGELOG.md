# Changelog

This project uses a `FEATURE.PATCH` (for instance *6.7*) versioning scheme, where `FEATURE` increases with mostly visible changes (interface, configuration) and `PATCH` increases with mostly invisible changes (performance, security, stability). Until sicuit leaves beta (see `sicuit -v`), breaking changes might occur as part of `FEATURE` releases should a strong need arise.

## 9.1

- Make the store migration introduced in 7.0 automatic (only ask confirmation)
- Minor improvements in --help and man page content 

## 9.0

- Sicuit now natively supports stores with multiple owners, including a new --for OWNER flag to set owner key(s) when intitializing or editing the store.

## 8.0

- Sicuit can now be used with any number of arbitrarily named and located store files through the use of an optional argument that overrides the store path.
- A critical double resource free regression in --init was fixed

## 7.0

- Transition to minimal, config-less storage convention
  * The encrypted secret file moves from `~/.sicuit/store.eno.gpg` to `~/.sicuit.eno.gpg`.
  *All else is now obsolete, therefore `~/.sicuit/config.eno` and `~/.sicuit/` can
  be deleted.
- More reliable temp file removal and resource cleanup on edit failure path
- Introduce sicuit(1) man page
- Add missing shortcut to --help

## 6.0

- Allow peeking at secrets within the query interface
- Display current position/total result numbers in top-right corner
- Add navigation with home/end keys
- Allow free cursor navigation, decoupled from paging state
- Prevent garbled input from unused delete key
- Expand --help instructions

## 5.0

- Switch to a cleanly modeled, recursive query architecture
- All text operations in the interface are now unicode-safe
- Visually separate hierarchy indicators from hidden keys
- Use new characterset to indicate hierarchy, truncation and obfuscation
- Remove offset column at the left margin
- Refine coloring/theme
- Refine truncation at right margin

## 4.0

- Query string input interaction is now unicode-proof
- Query string now truncates with ellipsis instead of overflowing
- Interface now uses the default terminal background color

## 3.0

- Update interface on window resize events
- Clip interface against right horizontal margin
- Reuse GPGME context in consecutive crypt operations

## 2.0

- Disregard accent, case and variant in store queries

## 1.2

- Handle alternate backspace key codes in query input

## 1.1

- Generate output for --version option at build time based on meson project version

## 1.0

- Initial versioned release
