# sicuit

Minimal plaintext password store

[![Screenshot](https://simonrepp.com/sicuit/screenshot.png)](https://simonrepp.com/sicuit/ "Screenshot")

## Installation and Usage

Visit [simonrepp.com/sicuit](https://simonrepp.com/sicuit)

## Development Priorities

*Sicuit is functionally finished*, the remaining priorities are:

- Polish and maximum stability for existing features
- Possibly extra security related additions such as idle-timer based lockout

## Contribution Pointers

- The most important contribution to make is simply to use sicuit and report issues when they occur
- Spread the word about sicuit to your community and peers if you think it could be of use to them
- If you have strong expertise in security, you can help out by auditing the code with respect to security
- Similarly if you have TUI development experience, you can help by pointing out possible terminal compatibility issues
- Additional migration scripts for other password managers will be met with open arms - see [importers](https://codeberg.org/simonrepp/sicuit/src/branch/main/importers) and [exporters](https://codeberg.org/simonrepp/sicuit/src/branch/main/exporters)

## Building from source

### Using meson (recommended)

1. Install `GPGME`, `ICU`, `meson` and `ncurses` from your package manager, if not already installed.

    ```bash
    # Arch Linux
    pacman -S gpgme icu meson ncurses

    # Ubuntu 20.04
    sudo apt install build-essential libgpgme-dev libicu-dev libncursesw5-dev meson

    # Other systems
    # Documentation contributions welcome!
    ```

2. Build and install [libeno](https://codeberg.org/simonrepp/libeno) from source

3. Open a terminal inside `sicuit/` and execute these commands:

    ```bash
    meson build
    cd build
    meson compile
    ```

4. After building you can optionally install sicuit to your system by running (inside `build/`):

    ```bash
    meson install
    ```

### Using CMake

Follow the build instructions for meson, with these differences:

- Install `cmake` instead of `meson`
- Replace the meson commands for building with these:

    ```bash
    mkdir build
    cd build
    cmake ..
    make
    ```
- There is no install target configured in the CMake build manifest yet

## License

Sicuit is licensed under the [GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html).
