# YOU ARE USING THIS SCRIPT ENTIRELY AT YOUR OWN RISK.
#
# Python 3.6+ and the 'enolib' package (pip install enolib) are required.
#
# This script reads ~/.sicuit.eno.gpg and writes ./sicuit_store_exported.json
# ALL YOUR SECRETS WILL BE EXPOSED AND OUT IN THE CLEAR TO ANYONE WHO CAN
# ACCCESS THAT FILE, so please make sure to quickly move that file/its content
# to a secure location and/or securely delete it ASAP after exporting.

# TODO: Export associated comments, best-effort style

# "import json" below would import this module itself
# instead of the python library module we want, therefore
# we remove the first entry from sys.path (the folder this
# script is in) so "import json" can not find and import
# this module, but imports the python library module instead
import sys
del sys.path[0]

import enolib, json, subprocess
from os import path

SICUIT_STORE_PATH = path.expanduser("~/.sicuit.eno.gpg")

decrypt_process = subprocess.run(
    ["gpg", "--decrypt", SICUIT_STORE_PATH],
    stderr=subprocess.DEVNULL,
    stdout=subprocess.PIPE,
    universal_newlines=True
)

store = enolib.parse(decrypt_process.stdout)

def recursive_export(element):
    result = {}

    for child in element.elements():
        if child.yields_section():
            section = child.to_section()
            result[section.string_key()] = recursive_export(section)
        elif child.yields_field():
            field = child.to_field()
            result[field.string_key()] = field.required_string_value()
        elif child.yields_fieldset():
            fieldset = child.to_fieldset()
            result[fieldset.string_key()] = {}

            for entry in fieldset.entries():
                result[fieldset.string_key()][entry.string_key()] = entry.optional_string_value()
        elif child.yields_list():
            list = child.to_list()
            result[list.string_key()] = list.optional_string_values()
        elif child.yields_empty():
            empty = child.to_empty()
            result[empty.string_key()] = None

    return result

result = recursive_export(store)

with open("sicuit_store_exported.json", "w") as file:
    file.write(json.dumps(result, indent=4, sort_keys=True))

print(f"[sicuit > json] All done, your secrets now lie UNENCRYPTED in 'sicuit_store_exported.json' inside this directory.")
