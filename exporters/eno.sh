#!/bin/bash

# YOU ARE USING THIS SCRIPT ENTIRELY AT YOUR OWN RISK.
#
# This script decrypts ~/.sicuit.eno.gpg to ./sicuit_store_exported.eno
# ALL YOUR SECRETS WILL BE EXPOSED AND OUT IN THE CLEAR TO ANYONE WHO CAN
# ACCCESS THAT FILE, so please make sure to quickly move that file/its content
# to a secure location and/or securely delete it ASAP after exporting.

gpg --output sicuit_store_exported.eno --decrypt ~/.sicuit.eno.gpg

echo "[sicuit > eno] All done, your secrets now lie UNENCRYPTED in 'sicuit_store_exported.eno' inside this directory."
