# YOU ARE USING THIS SCRIPT ENTIRELY AT YOUR OWN RISK.
# BACKUP YOUR PASS (RESPECTIVELY ALSO SICUIT) DATA BEFORE PROCEEDING.
#
# Python 3.6+ required
#
# This script reads ~/.password-store and writes ~/.sicuit.eno.gpg
# It asks your permission before overwriting anything.

import os, re, subprocess, sys
from os import path
from pathlib import Path

PASS_GPG_ID_PATH = path.expanduser("~/.password-store/.gpg-id")
PASS_DIR_PATH = path.expanduser("~/.password-store")
SICUIT_STORE_PATH = path.expanduser("~/.sicuit.eno.gpg")


def escape_key(key):
    backtick_matches = re.findall(r"`+", key)

    if backtick_matches:
        escape_len = max(len(match) for match in backtick_matches) + 1
    else:
        escape_len = 1

    return f"{'`' * escape_len}{key}{'`' * escape_len}"


def get_pass_gpg_id():
    try:
        with open(PASS_GPG_ID_PATH, "r") as file:
            return re.sub(r"\s", "", file.read())
    except IOError:
        return None


def import_pass_content():
    result = ""

    for dirpath, dirnames, filenames in os.walk(PASS_DIR_PATH):
        dirnames.sort()

        if dirpath != PASS_DIR_PATH:
            rel_dirpath_parts = path.relpath(dirpath, start=PASS_DIR_PATH).split(os.sep)
            key = rel_dirpath_parts[-1]
            operator = '#' * len(rel_dirpath_parts)

            result += f"{operator} {sanitize_section_key(key)}\n\n"

        if not filenames:
            continue

        filenames.sort()

        for filename in filenames:
            if filename == ".gpg-id":
                continue

            filepath = path.join(dirpath, filename)
            print(f"[pass > sicuit] INFO: Importing {filepath}")

            key = Path(filename).stem

            decrypt_process = subprocess.run(
                ["gpg", "--decrypt", filepath],
                stderr=subprocess.DEVNULL,
                stdout=subprocess.PIPE,
                universal_newlines=True
            )
            decrypted_lines = decrypt_process.stdout.splitlines()

            if len(decrypted_lines) > 1:
                additional_lines = "\n".join(decrypted_lines[1:]).strip().splitlines()
                for line in additional_lines:
                    result += f"> {line}\n"

            result += f"{sanitize_field_key(key)}: {decrypted_lines[0]}\n\n"

    return result


def sanitize_field_key(key):
    return escape_key(key) if re.search(r"^[\\|\-#`]|[:<]", key) else key


def sanitize_section_key(key):
    return escape_key(key) if re.search(r"^`|<", key) else key


def sicuit_store_writable():
    if not path.exists(SICUIT_STORE_PATH):
        return True

    while True:
        choice = input(f"[pass > sicuit] ACTION REQUIRED: Sicuit store at {SICUIT_STORE_PATH} already exists, overwrite? (y/N): ").lower()

        if not choice or choice == "n":
            return False
        if choice == "y":
            return True


def write_sicuit_store(content, gpg_id):
    Path(SICUIT_STORE_PATH).unlink(missing_ok=True)

    subprocess.run(
        ["gpg", "--encrypt", "-o", SICUIT_STORE_PATH, "-r", gpg_id],
        input=content,
        stderr=subprocess.DEVNULL,
        universal_newlines=True
    )


gpg_id = get_pass_gpg_id()

if not gpg_id:
    sys.exit(f"[pass > sicuit] ERROR: GPG ID could not be obtained from {PASS_GPG_ID_PATH}")

if not sicuit_store_writable():
    sys.exit(1)

content = import_pass_content()

write_sicuit_store(content, gpg_id)


print(f"[pass > sicuit] SUCCESS: All done, enjoy sicuit.")
